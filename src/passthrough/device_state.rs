use crate::filesystem::{DirectoryIterator, SerializableFileSystem};
use crate::fuse;
use crate::passthrough::inode_store::{
    Inode, InodeData, InodeIds, InodeMigrationInfo, InodeStore, RwLockInodeStoreHelpers,
    StrongInodeReference,
};
use crate::passthrough::stat::statx;
use crate::passthrough::util::openat;
use crate::passthrough::{FileOrHandle, Handle, HandleData, PassthroughFs};
use crate::read_dir::ReadDir;
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;
use std::convert::{TryFrom, TryInto};
use std::ffi::CStr;
use std::fs::File;
use std::io::{self, Read, Write};
use std::os::unix::io::{AsRawFd, FromRawFd};
use std::sync::atomic::{AtomicU64, Ordering};
use std::sync::{Arc, Mutex, RwLock};

#[derive(Debug, Deserialize, Serialize)]
pub struct PassthroughFsDeviceState {
    inodes: Vec<SerializableInodeData>,
    next_inode: u64,

    handles: Vec<SerializableHandleData>,
    next_handle: u64,

    writeback: bool,
    announce_submounts: bool,
    posix_acl: bool,
}

#[derive(Debug, Deserialize, Serialize)]
struct SerializableInodeData {
    /// ID
    pub inode: Inode,

    /// ID of the parent inode; ignored for the root node (`.inode == fuse::ROOT_ID`)
    pub parent: Inode,

    /// Current refcount
    pub refcount: u64,

    /// A filename relative to the parent that allows opening this inode.  Note that using `String`
    /// restricts us to paths that can be represented as UTF-8, which is not necessarily a
    /// restriction that all operating systems have.  However, we need to use some common encoding
    /// (i.e., cannot use `OsString`), or otherwise we could not migrate between operating systems
    /// using different string representations.
    /// Ignored for the root node (`.inode == fuse::ROOT_ID`).
    pub filename: String,
}

#[derive(Debug, Deserialize, Serialize)]
struct SerializableHandleData {
    handle: Handle,
    inode: Inode,
    flags: i32,
}

/// Recurses through a filesystem (i.e. the shared directory), looks up all entries (whether we
/// have those inodes registers), and sets `InodeData.migration_info` for each entry that we
/// recognize.
struct PathReconstructor {
    /// Reference to the filesystem for which to reconstruct inodes' paths.
    fs: Arc<PassthroughFs>,
}

impl PathReconstructor {
    fn new(fs: Arc<PassthroughFs>) -> Self {
        PathReconstructor { fs }
    }

    fn execute(self) -> io::Result<()> {
        self.fs.inodes.read().unwrap().clear_migration_info();
        let Ok(root) = self.fs.inodes.get_strong(&fuse::ROOT_ID) else {
            return Ok(()); // Nothing to do, shared dir is not mounted
        };

        self.recurse_from(root)
    }

    /// If this is a directory, it has its refcount increased by 1, so it will remain until
    /// serialization.  All serialized inodes are addressed relative to their parents, so all
    /// directories along any path must remain referenced.
    fn discover<F: AsRawFd>(
        &self,
        parent_reference: &StrongInodeReference,
        parent_fd: &F,
        name: &CStr,
    ) -> io::Result<Option<StrongInodeReference>> {
        let utf8_name = name.to_str().map_err(|err| {
            io::Error::new(
                io::ErrorKind::Other,
                format!("Cannot convert filename into UTF-8: {:?}: {err}", name),
            )
        })?;

        // Ignore these
        if utf8_name == "." || utf8_name == ".." {
            return Ok(None);
        }

        let path_fd = {
            let fd = self
                .fs
                .open_relative_to(parent_fd, name, libc::O_PATH, None)?;
            unsafe { File::from_raw_fd(fd) }
        };
        let stat = statx(&path_fd, None)?;
        let handle = self.fs.get_file_handle_opt(&path_fd, &stat)?;

        let ids = InodeIds {
            ino: stat.st.st_ino,
            dev: stat.st.st_dev,
            mnt_id: stat.mnt_id,
        };

        let is_directory = stat.st.st_mode & libc::S_IFMT == libc::S_IFDIR;

        // Be sure to limit the scope of the `.read()` lock
        {
            if let Ok(inode_ref) = self.fs.inodes.claim_inode(handle.as_ref(), &ids) {
                *inode_ref.get().migration_info.lock().unwrap() = Some(InodeMigrationInfo {
                    parent_ref: StrongInodeReference::clone(parent_reference),
                    name: utf8_name.to_string(),
                });

                return if is_directory {
                    Ok(Some(inode_ref))
                } else {
                    Ok(None)
                };
            }
        }

        if is_directory {
            let file_or_handle = if let Some(h) = handle.as_ref() {
                FileOrHandle::Handle(self.fs.make_file_handle_openable(h)?)
            } else {
                FileOrHandle::File(path_fd)
            };

            let new_inode = InodeData {
                inode: self.fs.next_inode.fetch_add(1, Ordering::Relaxed),
                file_or_handle,
                refcount: AtomicU64::new(1),
                ids,
                mode: stat.st.st_mode,
                migration_info: Mutex::new(Some(InodeMigrationInfo {
                    parent_ref: StrongInodeReference::clone(parent_reference),
                    name: utf8_name.to_string(),
                })),
            };

            Ok(Some(self.fs.inodes.new_inode(new_inode)))
        } else {
            Ok(None)
        }
    }

    fn recurse_from(&self, inode_ref: StrongInodeReference) -> io::Result<()> {
        let dirfd = inode_ref.get().open_file(
            libc::O_RDONLY | libc::O_NOFOLLOW | libc::O_CLOEXEC,
            &self.fs.proc_self_fd,
        )?;
        let mut dir_buf = vec![0u8; 1024];
        let mut in_dir_ofs = 0i64;

        loop {
            let mut entries = ReadDir::new(&dirfd, in_dir_ofs, dir_buf.as_mut())?;
            if entries.remaining() == 0 {
                return Ok(());
            }
            in_dir_ofs += entries.remaining() as i64;

            while let Some(entry) = entries.next() {
                if !self.fs.track_path_names.load(Ordering::Relaxed) {
                    // If `track_path_names` becomes false, we should stop
                    return Ok(());
                }

                if let Some(entry_inode) = self.discover(&inode_ref, &dirfd, entry.name)? {
                    self.recurse_from(entry_inode)?;
                }
            }
        }
    }
}

impl SerializableFileSystem for PassthroughFs {
    fn prepare_serialization(self: &Arc<Self>) -> io::Result<()> {
        self.track_path_names.store(true, Ordering::Relaxed);

        let cloned_fs = Arc::clone(self);
        let mut thread_handle_mutex_guard = self.serialization_prepare_thread.lock().unwrap();

        if thread_handle_mutex_guard.is_some() {
            return Err(io::Error::new(
                io::ErrorKind::Other,
                "Already preparing for serialization".to_string(),
            ));
        }

        let reconstructor = PathReconstructor::new(cloned_fs);
        let thread_handle = std::thread::spawn(move || reconstructor.execute());

        *thread_handle_mutex_guard = Some(thread_handle);
        Ok(())
    }

    fn serialize(&self, mut state_pipe: File) -> io::Result<()> {
        if let Some(thread) = self.serialization_prepare_thread.lock().unwrap().take() {
            // It isn't great to have this vhost-user message block, but if the preparations are
            // still ongoing, we have no choice
            thread.join().map_err(|_| {
                io::Error::new(
                    io::ErrorKind::Other,
                    "Failed to finalize serialization preparation".to_string(),
                )
            })??;
        }

        self.track_path_names.store(false, Ordering::Relaxed);

        let state: PassthroughFsDeviceState = self.try_into()?;
        let serialized: Vec<u8> = state.try_into()?;
        state_pipe.write_all(&serialized)?;
        Ok(())
    }

    fn deserialize_and_apply(&self, mut state_pipe: File) -> io::Result<()> {
        let mut serialized: Vec<u8> = Vec::new();
        state_pipe.read_to_end(&mut serialized)?;
        let state: PassthroughFsDeviceState = serialized.try_into()?;
        state.apply(self)?;
        Ok(())
    }

    fn cancel_migration(&self) {
        self.track_path_names.store(false, Ordering::Relaxed);
        if let Some(thread) = self.serialization_prepare_thread.lock().unwrap().take() {
            // Ignore result, but do wait on it to be settled (should be done very quickly, since
            // `recurse_from()` checks `track_path_names` every iteration)
            let _: std::thread::Result<io::Result<()>> = thread.join();
        }
        self.inodes.read().unwrap().clear_migration_info();
    }
}

impl PassthroughFsDeviceState {
    pub fn apply(mut self, fs: &PassthroughFs) -> io::Result<()> {
        if !fs.cfg.writeback && self.writeback {
            return Err(io::Error::new(
                io::ErrorKind::Other,
                "Migration source wants writeback enabled, but it is disabled on the destination",
            ));
        } else {
            // We can safely disable writeback, because that is done anyway if the guest does not
            // support it
            fs.writeback.store(self.writeback, Ordering::Relaxed);
        }

        if !fs.cfg.announce_submounts && self.announce_submounts {
            return Err(io::Error::new(
                io::ErrorKind::Other,
                "Migration source wants announce-submounts enabled, but it is disabled on the \
                 destination",
            ));
        } else {
            // We can safely disable announce-submounts, because that is done anyway if the guest
            // does not support it
            fs.announce_submounts
                .store(self.announce_submounts, Ordering::Relaxed);
        }

        if !fs.cfg.posix_acl && self.posix_acl {
            return Err(io::Error::new(
                io::ErrorKind::Other,
                "Migration source wants posix ACLs enabled, but it is disabled on the destination",
            ));
        } else {
            // We can safely disable posix ACLs, because that is done anyway if the guest does not
            // support it
            fs.posix_acl.store(self.posix_acl, Ordering::Relaxed);
        }

        *fs.inodes.write().unwrap() = InodeStore::default();

        // Inodes are serialized as filenames in directories, referencing those parent directories.
        // We must deserialize the parent directory before its entries, so repeatedly iterate
        // through the inode list, until we are able to deserialize everything.
        while !self.inodes.is_empty() {
            let mut i = 0;
            let mut processed_any = false;
            while i < self.inodes.len() {
                if self.inodes[i].inode == fuse::ROOT_ID {
                    // We open the root node ourselves (from the configuration the user gave us)...
                    fs.open_root_node()?;

                    // ...and only take the refcount from the source, ignoring filename and parent
                    // information.  Note that we must not call `fs.open_root_node()` before we
                    // have the correct refcount, or opening child nodes via `.try_into_with_fs()`
                    // (which drops one reference each) would quickly reduce the refcount below 0.
                    fs.inodes
                        .read()
                        .unwrap()
                        .get(&fuse::ROOT_ID)
                        .unwrap()
                        .refcount
                        .store(self.inodes[i].refcount, Ordering::Relaxed);
                    self.inodes.remove(i);
                    processed_any = true;
                } else if let Some(inode_data) = self.inodes[i].try_into_with_fs(fs)? {
                    fs.inodes.write().unwrap().insert(Arc::new(inode_data));
                    self.inodes.remove(i);
                    processed_any = true;
                } else {
                    // Parent not found, continue.
                    i += 1;
                }
            }

            if !processed_any {
                return Err(io::Error::new(
                    io::ErrorKind::Other,
                    "Unresolved parent references",
                ));
            }
        }

        fs.next_inode.store(self.next_inode, Ordering::Relaxed);

        let mut handles = BTreeMap::new();
        for handle in self.handles {
            handles.insert(handle.handle, Arc::new(handle.try_into_with_fs(fs)?));
        }

        *fs.handles.write().unwrap() = handles;
        fs.next_handle.store(self.next_handle, Ordering::Relaxed);

        Ok(())
    }
}

impl TryFrom<&PassthroughFs> for PassthroughFsDeviceState {
    type Error = io::Error;

    fn try_from(fs: &PassthroughFs) -> io::Result<Self> {
        let inodes_map = fs.inodes.read().unwrap();
        let handles_map = fs.handles.read().unwrap();

        let inodes = inodes_map
            .iter()
            .map(|inode| inode.as_ref().try_into())
            .collect::<io::Result<Vec<SerializableInodeData>>>()?;

        let handles = handles_map
            .iter()
            .map(|(handle, data)| (*handle, data.as_ref()).into())
            .collect();

        Ok(PassthroughFsDeviceState {
            inodes,
            next_inode: fs.next_inode.load(Ordering::Relaxed),

            handles,
            next_handle: fs.next_handle.load(Ordering::Relaxed),

            writeback: fs.writeback.load(Ordering::Relaxed),
            announce_submounts: fs.announce_submounts.load(Ordering::Relaxed),
            posix_acl: fs.posix_acl.load(Ordering::Relaxed),
        })
    }
}

impl TryFrom<&InodeData> for SerializableInodeData {
    type Error = io::Error;

    fn try_from(data: &InodeData) -> io::Result<Self> {
        let inode = data.inode;
        let refcount = data.refcount.load(Ordering::Relaxed);
        let (parent, filename) = if inode != fuse::ROOT_ID {
            let migration_info_locked = data.migration_info.lock().unwrap();
            let migration_info = migration_info_locked.as_ref().ok_or_else(|| {
                io::Error::new(
                    io::ErrorKind::Other,
                    format!(
                        "Cannot serialize inode {} (st_dev={}, mnt_id={}, st_ino={}): No path found",
                        data.inode, data.ids.dev, data.ids.mnt_id, data.ids.ino,
                    ),
                )
            })?;

            // Safe: We serialize everything before we dropping the serialized state, so the strong
            // refcount will effectively outlive this weak reference
            let parent = unsafe { migration_info.parent_ref.get_raw() };
            let filename = migration_info.name.clone();

            let parent = parent.ok_or_else(|| {
                io::Error::new(
                    io::ErrorKind::Other,
                    format!(
                        "Inode {} (st_dev={}, mnt_id={}, st_ino={}) has no parent information",
                        data.inode, data.ids.dev, data.ids.mnt_id, data.ids.ino
                    ),
                )
            })?;

            (parent, filename)
        } else {
            // We do not serialize how to find the root node, the destination will need to find it
            // on its own.
            (0, "".into())
        };

        Ok(SerializableInodeData {
            inode,
            parent,
            refcount,
            filename,
        })
    }
}

impl SerializableInodeData {
    fn try_into_with_fs(&self, fs: &PassthroughFs) -> io::Result<Option<InodeData>> {
        let parent_data = match fs.inodes.read().unwrap().get(&self.parent).map(Arc::clone) {
            Some(data) => data,
            None => return Ok(None),
        };

        // Safe because the migration source guarantees that this reference is included in the
        // parent node's refcount.  Once we have deserialized this inode, we must drop that
        // reference, and dropping this object will achieve that.
        let parent_ref =
            unsafe { StrongInodeReference::new_no_increment(parent_data, Arc::clone(&fs.inodes)) };

        let parent_fd = parent_ref.get().get_file()?;
        let fd = openat(
            &parent_fd,
            &self.filename,
            libc::O_PATH | libc::O_NOFOLLOW | libc::O_CLOEXEC,
        )
        .map_err(|err| {
            io::Error::new(
                err.kind(),
                format!(
                    "Opening {{fd:{}}}/{}: {}",
                    parent_fd.as_raw_fd(),
                    self.filename,
                    err
                ),
            )
        })?;

        let st = statx(&fd, None)?;
        let handle = fs.get_file_handle_opt(&fd, &st)?;

        let file_or_handle = if let Some(h) = handle.as_ref() {
            FileOrHandle::Handle(fs.make_file_handle_openable(h)?)
        } else {
            FileOrHandle::File(fd)
        };

        Ok(Some(InodeData {
            inode: self.inode,
            file_or_handle,
            refcount: AtomicU64::new(self.refcount),
            ids: InodeIds {
                ino: st.st.st_ino,
                dev: st.st.st_dev,
                mnt_id: st.mnt_id,
            },
            mode: st.st.st_mode,
            migration_info: Mutex::new(None),
        }))
    }
}

impl From<(Handle, &HandleData)> for SerializableHandleData {
    fn from(handle: (Handle, &HandleData)) -> Self {
        SerializableHandleData {
            handle: handle.0,
            inode: handle.1.inode,
            flags: handle.1.flags,
        }
    }
}

impl SerializableHandleData {
    fn try_into_with_fs(&self, fs: &PassthroughFs) -> io::Result<HandleData> {
        let inode_store = fs.inodes.read().unwrap();
        let inode = inode_store.get(&self.inode).ok_or_else(|| {
            io::Error::new(
                io::ErrorKind::Other,
                format!("Inode {} not found", self.inode),
            )
        })?;
        let file = inode.open_file(self.flags, &fs.proc_self_fd)?.into_file()?;

        Ok(HandleData {
            inode: self.inode,
            file: RwLock::new(file),
            flags: self.flags,
        })
    }
}

impl TryFrom<PassthroughFsDeviceState> for Vec<u8> {
    type Error = io::Error;

    fn try_from(state: PassthroughFsDeviceState) -> io::Result<Self> {
        postcard::to_stdvec(&state).map_err(|err| io::Error::new(io::ErrorKind::Other, err))
    }
}

impl TryFrom<Vec<u8>> for PassthroughFsDeviceState {
    type Error = io::Error;

    fn try_from(serialized: Vec<u8>) -> io::Result<Self> {
        postcard::from_bytes(&serialized).map_err(|err| io::Error::new(io::ErrorKind::Other, err))
    }
}
