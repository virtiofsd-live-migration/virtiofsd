// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE-BSD-3-Clause file.

use crate::passthrough::file_handle::{FileHandle, FileOrHandle};
use crate::passthrough::forget_one;
use crate::passthrough::stat::MountId;
use crate::passthrough::util::{ebadf, is_safe_inode, reopen_fd_through_proc};
use std::collections::{btree_map, BTreeMap};
use std::ffi::CStr;
use std::fs::File;
use std::io;
use std::os::unix::io::{AsRawFd, RawFd};
use std::sync::atomic::{AtomicU64, Ordering};
use std::sync::{Arc, Mutex, RwLock};

pub type Inode = u64;

#[derive(Clone, Copy, Eq, Ord, PartialEq, PartialOrd)]
pub struct InodeIds {
    pub ino: libc::ino64_t,
    pub dev: libc::dev_t,
    pub mnt_id: MountId,
}

pub struct InodeMigrationInfo {
    pub parent_ref: StrongInodeReference,
    pub name: String,
}

pub struct StrongInodeReference {
    /// May be `None` if the inode has already been leaked, or this is a pseudo-reference (i.e.
    /// empty)
    inode_data: Option<Arc<InodeData>>,

    /// May only be `None` if `inode` is `None`
    inode_store: Option<Arc<RwLock<InodeStore>>>,
}

pub struct InodeData {
    pub inode: Inode,
    // Most of these aren't actually files but ¯\_(ツ)_/¯.
    pub file_or_handle: FileOrHandle,
    pub refcount: AtomicU64,

    // Used as key in the `InodeStore::by_ids` map.
    pub ids: InodeIds,

    // File type and mode
    pub mode: u32,

    // Used during migration
    pub migration_info: Mutex<Option<InodeMigrationInfo>>,
}

/**
 * Represents the file associated with an inode (`InodeData`).
 *
 * When obtaining such a file, it may either be a new file (the `Owned` variant), in which case the
 * object's lifetime is static, or it may reference `InodeData.file` (the `Ref` variant), in which
 * case the object's lifetime is that of the respective `InodeData` object.
 */
pub enum InodeFile<'inode_lifetime> {
    Owned(File),
    Ref(&'inode_lifetime File),
}

#[derive(Default)]
pub struct InodeStore {
    data: BTreeMap<Inode, Arc<InodeData>>,
    by_ids: BTreeMap<InodeIds, Inode>,
    by_handle: BTreeMap<FileHandle, Inode>,
}

impl<'a> InodeData {
    /// Get an `O_PATH` file for this inode
    pub fn get_file(&'a self) -> io::Result<InodeFile<'a>> {
        match &self.file_or_handle {
            FileOrHandle::File(f) => Ok(InodeFile::Ref(f)),
            FileOrHandle::Handle(h) => {
                let file = h.open(libc::O_PATH)?;
                Ok(InodeFile::Owned(file))
            }
        }
    }

    /// Open this inode with the given flags
    /// (always returns a new (i.e. `Owned`) file, hence the static lifetime)
    pub fn open_file(
        &self,
        flags: libc::c_int,
        proc_self_fd: &File,
    ) -> io::Result<InodeFile<'static>> {
        if !is_safe_inode(self.mode) {
            return Err(ebadf());
        }

        match &self.file_or_handle {
            FileOrHandle::File(f) => {
                let new_file = reopen_fd_through_proc(f, flags, proc_self_fd)?;
                Ok(InodeFile::Owned(new_file))
            }
            FileOrHandle::Handle(h) => {
                let new_file = h.open(flags)?;
                Ok(InodeFile::Owned(new_file))
            }
        }
    }
}

impl InodeFile<'_> {
    /// Create a standalone `File` object
    pub fn into_file(self) -> io::Result<File> {
        match self {
            Self::Owned(file) => Ok(file),
            Self::Ref(file_ref) => file_ref.try_clone(),
        }
    }
}

impl AsRawFd for InodeFile<'_> {
    /// Return a file descriptor for this file
    /// Note: This fd is only valid as long as the `InodeFile` exists.
    fn as_raw_fd(&self) -> RawFd {
        match self {
            Self::Owned(file) => file.as_raw_fd(),
            Self::Ref(file_ref) => file_ref.as_raw_fd(),
        }
    }
}

impl InodeStore {
    pub fn insert(&mut self, data: Arc<InodeData>) {
        self.by_ids.insert(data.ids, data.inode);
        if let FileOrHandle::Handle(handle) = &data.file_or_handle {
            self.by_handle.insert(handle.inner().clone(), data.inode);
        }
        self.data.insert(data.inode, data);
    }

    pub fn remove(&mut self, inode: &Inode) -> Option<Arc<InodeData>> {
        let data = self.data.remove(inode);
        if let Some(data) = data.as_ref() {
            if let FileOrHandle::Handle(handle) = &data.file_or_handle {
                self.by_handle.remove(handle.inner());
            }
            self.by_ids.remove(&data.ids);
        }
        data
    }

    pub fn clear(&mut self) {
        self.data.clear();
        self.by_handle.clear();
        self.by_ids.clear();
    }

    pub fn clear_migration_info(&self) {
        for inode in self.data.values() {
            inode.migration_info.lock().unwrap().take();
        }
    }

    pub fn get(&self, inode: &Inode) -> Option<&Arc<InodeData>> {
        self.data.get(inode)
    }

    pub fn get_by_ids(&self, ids: &InodeIds) -> Option<&Arc<InodeData>> {
        self.inode_by_ids(ids).map(|inode| self.get(inode).unwrap())
    }

    pub fn get_by_handle(&self, handle: &FileHandle) -> Option<&Arc<InodeData>> {
        self.inode_by_handle(handle)
            .map(|inode| self.get(inode).unwrap())
    }

    pub fn inode_by_ids(&self, ids: &InodeIds) -> Option<&Inode> {
        self.by_ids.get(ids)
    }

    pub fn inode_by_handle(&self, handle: &FileHandle) -> Option<&Inode> {
        self.by_handle.get(handle)
    }

    pub fn iter(&self) -> btree_map::Values<'_, Inode, Arc<InodeData>> {
        self.data.values()
    }

    /// Helper for Arc<RwLock<InodeData>>::claim_inode()
    pub fn do_claim_inode(
        &self,
        self_arc: &Arc<RwLock<Self>>,
        handle: Option<&FileHandle>,
        ids: &InodeIds,
    ) -> io::Result<StrongInodeReference> {
        let data = handle
            .and_then(|h| self.get_by_handle(h))
            .or_else(|| {
                self.get_by_ids(ids).filter(|data| {
                    // When we have to fall back to looking up an inode by its inode ID, ensure
                    // that we hit an entry that has a valid file descriptor.  Having an FD
                    // open means that the inode cannot really be deleted until the FD is
                    // closed, so that the inode ID remains valid until we evict the
                    // `InodeData`.  With no FD open (and just a file handle), the inode can be
                    // deleted while we still have our `InodeData`, and so the inode ID may be
                    // reused by a completely different new inode.  Such inodes must be looked
                    // up by file handle, because this handle contains a generation ID to
                    // differentiate between the old and the new inode.
                    matches!(data.file_or_handle, FileOrHandle::File(_))
                })
            })
            .ok_or_else(|| {
                io::Error::new(
                    io::ErrorKind::NotFound,
                    "Cannot take strong reference to inode by handle or IDs, not found".to_string(),
                )
            })?;

        StrongInodeReference::new_with_data(Arc::clone(data), Arc::clone(self_arc))
    }
}

/// Helper functions to implement on `Arc<RwLock<InodeStore>>`.
pub trait RwLockInodeStoreHelpers {
    /// Turn the weak reference `inode` into a strong one (increments its refcount)
    fn get_strong(&self, inode: &Inode) -> io::Result<StrongInodeReference>;

    /// Attempts to get an inode from `inodes` and increment its refcount.  Returns the inode
    /// number on success and `None` on failure.  Reasons for failure can be that the inode isn't
    /// in the map or that the refcount is zero.  This function will never increment a refcount
    /// that's already zero.
    fn claim_inode(
        &self,
        handle: Option<&FileHandle>,
        ids: &InodeIds,
    ) -> io::Result<StrongInodeReference>;

    /// Locks itself, and first double-checks whether a matching inode is already present (see
    /// `claim_inode`).  If so, that inode is returned and `inode_data` is dropped.  Otherwise,
    /// `inode_data` is inserted.
    fn new_inode(&self, inode_data: InodeData) -> StrongInodeReference;
}

impl RwLockInodeStoreHelpers for Arc<RwLock<InodeStore>> {
    fn get_strong(&self, inode: &Inode) -> io::Result<StrongInodeReference> {
        StrongInodeReference::new(*inode, Arc::clone(self))
    }

    fn claim_inode(
        &self,
        handle: Option<&FileHandle>,
        ids: &InodeIds,
    ) -> io::Result<StrongInodeReference> {
        self.read().unwrap().do_claim_inode(self, handle, ids)
    }

    fn new_inode(&self, mut inode_data: InodeData) -> StrongInodeReference {
        let mut locked = self.write().unwrap();
        let handle = match &inode_data.file_or_handle {
            FileOrHandle::File(_) => None,
            FileOrHandle::Handle(handle) => Some(handle.inner()),
        };
        if let Ok(inode) = locked.do_claim_inode(self, handle, &inode_data.ids) {
            return inode;
        }

        // Safe because we have the only reference
        inode_data.refcount = AtomicU64::new(1);
        let inode_data = Arc::new(inode_data);
        locked.insert(Arc::clone(&inode_data));

        // We just set the reference to 1 to account for this
        unsafe { StrongInodeReference::new_no_increment(inode_data, Arc::clone(self)) }
    }
}

impl StrongInodeReference {
    /// Do not increment the inode's refcount, assume caller did it
    ///
    /// # Safety
    /// Caller ensures the inode's refcount is incremented by 1 to account for this strong
    /// reference.
    pub unsafe fn new_no_increment(
        inode_data: Arc<InodeData>,
        inode_store: Arc<RwLock<InodeStore>>,
    ) -> Self {
        StrongInodeReference {
            inode_data: Some(inode_data),
            inode_store: Some(inode_store),
        }
    }

    fn increment_refcount_for(inode_data: &InodeData) -> io::Result<()> {
        let mut old_refcount = inode_data.refcount.load(Ordering::Relaxed);

        // We use a CAS loop instead of `fetch_add()`, because we must never increment the refcount
        // from zero to one.
        loop {
            if old_refcount == 0 {
                return Err(io::Error::new(
                    io::ErrorKind::NotFound,
                    format!(
                        "Cannot take strong reference to inode {}: Is already deleted",
                        inode_data.inode
                    ),
                ));
            }

            match inode_data.refcount.compare_exchange(
                old_refcount,
                old_refcount + 1,
                Ordering::Relaxed,
                Ordering::Relaxed,
            ) {
                Ok(_) => return Ok(()),
                Err(refcount) => old_refcount = refcount,
            }
        }
    }

    pub fn new_with_data(
        inode_data: Arc<InodeData>,
        inode_store: Arc<RwLock<InodeStore>>,
    ) -> io::Result<Self> {
        Self::increment_refcount_for(&inode_data)?;

        // Safe because we have just incremented the refcount
        Ok(unsafe { StrongInodeReference::new_no_increment(inode_data, inode_store) })
    }

    pub fn new(inode: Inode, inode_store: Arc<RwLock<InodeStore>>) -> io::Result<Self> {
        let inode_data = inode_store
            .read()
            .unwrap()
            .get(&inode)
            .ok_or_else(|| {
                io::Error::new(
                    io::ErrorKind::NotFound,
                    format!("Cannot take strong reference to inode {inode}: Not found"),
                )
            })
            .map(Arc::clone)?;

        Self::new_with_data(inode_data, inode_store)
    }

    pub fn empty() -> Self {
        StrongInodeReference {
            inode_data: None,
            inode_store: None,
        }
    }

    /// Consume this strong reference, yield the underlying inode ID, without decrementing the
    /// inode's refcount.
    ///
    /// # Safety
    /// Caller must guarantee that the refcount is tracked somehow still, i.e. that forget_one()
    /// will eventually be called, or this inode is truly leaked
    pub unsafe fn leak(mut self) -> Option<Inode> {
        self.inode_data.take().map(|data| data.inode)
    }

    /// Yield the underlying inode ID.
    ///
    /// # Safety
    /// The inode ID is technically a form of a weak reference.  To ensure safety, the caller may
    /// not assume that it is valid beyond the lifetime of the corresponding strong reference.
    pub unsafe fn get_raw(&self) -> Option<Inode> {
        self.inode_data.as_ref().map(|data| data.inode)
    }

    /// Do not call on an empty inode.
    pub fn get(&self) -> &InodeData {
        // Caller guarantees that this will not panic
        self.inode_data.as_ref().unwrap()
    }
}

impl Clone for StrongInodeReference {
    fn clone(&self) -> Self {
        if let Some(inode_data) = self.inode_data.as_ref() {
            // Safe: If `self.inode_data` is not `None`, `self.inode_store` must be set
            let cloned_store = Arc::clone(self.inode_store.as_ref().unwrap());
            // Safe: Can only fail if the refcount became 0, but we have a strong reference already
            Self::new_with_data(Arc::clone(inode_data), cloned_store).unwrap()
        } else {
            Self::empty()
        }
    }
}

impl Drop for StrongInodeReference {
    fn drop(&mut self) {
        if let Some(inode_data) = self.inode_data.take() {
            let mut inodes = self.inode_store.as_ref().unwrap().write().unwrap();
            forget_one(&mut inodes, inode_data.inode, 1);
        }
    }
}

impl InodeMigrationInfo {
    pub fn new(parent_ref: StrongInodeReference, filename: &CStr) -> io::Result<Self> {
        let utf8_name = filename.to_str().map_err(|err| {
            io::Error::new(
                io::ErrorKind::Other,
                format!("Cannot convert filename into UTF-8: {filename:?}: {err}"),
            )
        })?;

        Ok(InodeMigrationInfo {
            parent_ref,
            name: utf8_name.to_string(),
        })
    }
}
